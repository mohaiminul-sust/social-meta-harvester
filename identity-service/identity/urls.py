"""identity URL Configuration"""

from django.conf import settings
from django.conf.urls import handler404, url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, reverse_lazy, re_path
from django.views.generic.base import RedirectView

from rest_framework.documentation import include_docs_urls
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

admin.site.site_header = "Identity Service"
admin.site.site_title = "Identity"
admin.site.index_title = "Dashboard"

doc_prefernces = {
    'title': 'Identity Service',
    'description': 'API Documentation for Client Integration'
}

openapi_schema_view = get_schema_view(
   openapi.Info(
      title=doc_prefernces['title'],
      default_version='1.0.1',
      description=doc_prefernces['description'],
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@metagen.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[ permissions.AllowAny ],
)

handler404 = 'user.views.error_404_view'

urlpatterns = [
    url(r'^$', RedirectView.as_view(url=reverse_lazy('admin:index'))),
    path('admin/', include('smuggler.urls')),
    path('admin/', admin.site.urls),

    path(r'', include('user.urls')),

    url(r'^filer/', include('filer.urls')),
    # re_path(r'api-docs/', include_docs_urls(**doc_prefernces)),
    re_path(r'^docs(?P<format>\.json|\.yaml)$', openapi_schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^docs/$', openapi_schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', openapi_schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

if bool(settings.DEBUG):
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
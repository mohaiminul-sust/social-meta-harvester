from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from django.utils.http import urlencode
from django.core import serializers
from django.utils.crypto import get_random_string
from django.template import Context, loader
from django.conf import settings

from rest_framework import generics, status, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, NotFound, NotAcceptable
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view

from rest_auth.registration.views import LoginView, RegisterView, VerifyEmailView

from rest_framework_simplejwt.views import TokenObtainPairView

from .models import User, PersonalInfo
from .serializers import UserSerializer, PersonalInfoSerializer, AccountRegisterSerializer, JwtTokenObtainPairSerializer
from .utils import get_parts_from_name

#API Views
class RestRegisterView(RegisterView):
    authentication_classes = (TokenAuthentication,)
    serializer_class = AccountRegisterSerializer


class RestLoginView(LoginView):
    authentication_classes = (TokenAuthentication,)


class RestVerifyEmailView(VerifyEmailView):
    authentication_classes = (TokenAuthentication,)


@api_view()
def null_view(request):
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view()
def complete_view(request):
    return Response("Email account is activated")


class PersonalInfoViewSet(generics.RetrieveUpdateAPIView):
    authentication_classes = (TokenAuthentication,)
    queryset = PersonalInfo.objects.all()
    serializer_class = PersonalInfoSerializer

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.get(user=self.request.user)
        if obj:
            self.check_object_permissions(self.request, obj)
            return obj
        else:
            raise NotFound("No user data found!")

    def perform_update(self, serializer):
        instance = serializer.save(user=self.request.user)
        return instance


class JwtTokenObtainPairView(TokenObtainPairView):
    serializer_class = JwtTokenObtainPairSerializer
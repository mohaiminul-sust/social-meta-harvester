from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.authtoken.models import Token
from rest_auth.registration.serializers import RegisterSerializer

from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm
# from allauth.account.forms import ResetPasswordForm, SetPasswordForm
from django.conf import settings
from django.utils.translation import gettext as _
# from django.utils.encoding import force_text
# from django.utils.http import urlsafe_base64_decode as uid_decoder

# from allauth.account.forms import default_token_generator
# from allauth.account.utils import url_str_to_user_pk
# import sys
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from .models import User, PersonalInfo
from .utils import get_parts_from_name


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        extra_kwargs = {
            'password': {
                'write_only': True
            }
        }

    def create(self, validated_data):
        user = User(
            email=validated_data['email']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class PersonalInfoSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField()
    last_login = serializers.DateTimeField(source="user__last_login", format="%Y-%m-%d %I:%M:%S %p", read_only=True)
    class Meta:
        model = PersonalInfo
        # fields = '__all__'
        exclude = ('user', 'created_date', 'updated_date',)

    def get_email(self, obj):
        return obj.user.email

    def validate_data(self, data):
        full_name = data.pop('full_name')
        if full_name:
            full_name = str(full_name).strip()
            first, last = get_parts_from_name(full_name)
            data['first_name'] = first
            data['last_name'] = last

        return data


class AccountRegisterSerializer(RegisterSerializer):
    def custom_signup(self, request, user):
        full_name = request.data.get('full_name', None)
        if full_name is None:
            user.save()
        else:
            full_name = str(full_name).strip()
            first, last = get_parts_from_name(full_name)
            user.first_name = first
            user.last_name = last
            user.save(update_fields=['first_name', 'last_name'])


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password_reset_form_class = PasswordResetForm

    def validate_email(self, value):
        self.reset_form = self.password_reset_form_class(data=self.initial_data)
        if not self.reset_form.is_valid():
            raise serializers.ValidationError(_('Error'))

        ###### FILTER USER MODEL ######
        if not User.objects.filter(email=value).exists():
            raise serializers.ValidationError(_('Invalid e-mail address'))
        return value

    def save(self):
        request = self.context.get('request')
        opts = {
            'use_https': request.is_secure(),
            'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),
            'email_template_name': 'account/email/password_reset_key_message.txt',
            'request': request,
        }
        self.reset_form.save(**opts)

# class PasswordResetConfirmSerializer(serializers.Serializer):
#     """
#     Serializer for requesting a password reset e-mail.
#     """
#     new_password1 = serializers.CharField(max_length=128)
#     new_password2 = serializers.CharField(max_length=128)
#     uid = serializers.CharField()
#     token = serializers.CharField()

#     set_password_form_class = SetPasswordForm

#     def custom_validation(self, attrs):
#         pass

#     def validate(self, attrs):
#         self._errors = {}

#         # Decode the uidb64 to uid to get User object
#         try:
#             # uid = force_text(uid_decoder(attrs['uid']))
#             uid = url_str_to_user_pk(attrs['uid'])
#             self.user = User.objects.get(pk=uid)
#         except (TypeError, ValueError, OverflowError, User.DoesNotExist):
#             raise ValidationError({'uid': ['Invalid value']})

#         self.custom_validation(attrs)
#         # Construct SetPasswordForm instance
#         self.set_password_form = self.set_password_form_class(
#             user=self.user, data=attrs
#         )
#         if not self.set_password_form.is_valid():
#             raise serializers.ValidationError(self.set_password_form.errors)
#         if not default_token_generator.check_token(self.user, attrs['token']):
#             raise ValidationError({'token': ['Invalid value']})

#         return attrs

#     def save(self):
#         return self.set_password_form.save()


class JwtTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token['fullname'] = user.personal_info.full_name()
        token['username'] = user.username
        token['email'] = user.email
        token['user_role'] = user.assigned_group()
        # ...

        return token
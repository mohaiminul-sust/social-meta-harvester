from django.conf import settings
from django.utils.html import mark_safe
from django.contrib.sites.models import Site
from django.core.files.images import get_image_dimensions
from django.contrib import admin
import os
from django.utils.timezone import now
from datetime import timedelta


def get_formatted_name(text):
    l_cased = text.lower()
    name_str = l_cased[0].upper() + l_cased[1:len(l_cased)]
    return name_str


def get_parts_from_name(full_name):
    if full_name.strip() == '':
        return ('', '')
    name_arr = full_name.split(" ", 1)
    first = get_formatted_name(name_arr[0])
    last = get_formatted_name(name_arr[1]) if len(name_arr) > 1 else ""
    return (first, last)


def get_current_domain():
    return Site.objects.get_current().domain


def asset_url_for(path):
    if not str(path) == "":
        return get_current_domain() + os.path.join(settings.MEDIA_URL, str(path))
    else:
        return None


def filer_url_for(obj):
    return get_current_domain() + obj.url if obj is not None else None


def image_component(obj):
    if obj is None:
        return None

    width, height = get_image_dimensions(obj.file)
    if height > 35:
        ratio = 35/height
        width = ratio*width
        height = ratio*height
    return mark_safe('<img src="%s" width="%d" height="%d" />' % (obj.url, width, height))
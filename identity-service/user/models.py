from django.db import models
from django.db.models import Sum
from uuid import uuid4
from django.contrib.auth.models import AbstractUser

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.utils.timezone import now
from django.utils.translation import gettext as _
from django.core.validators import RegexValidator
from django.conf import settings

from .user_manager import UserManager


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    email = models.EmailField(max_length=254, unique=True)
    avatar = models.ImageField(upload_to='user_avatars', null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    def __str__(self):
        return self.email if self.email.strip() != '' else str(self.id)

    def assigned_group(self):
        return self.groups.values_list('name', flat = True)[0] if self.groups.exists() else '-'

    def can_receive_sms(self):
        return self.personal_info.is_otp_verified if self.personal_info and not self.personal_info.contact_number == '' else False

    def sms_number(self):
        return self.personal_info.contact_number



class PersonalInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, related_name='personal_info')

    first_name = models.CharField(max_length=150, blank=True, null=True, help_text="First Name")

    last_name = models.CharField(max_length=150, blank=True, null=True, help_text="Last Name")

    date_of_birth = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True, help_text="Birthday")

    # contact_regex = RegexValidator(regex=settings.PHONE_VALIDATION_REGEX, message="Contact number must be entered in the format: '+999999999'. Up to 15 digits allowed.")

    # contact_number = models.CharField(validators=[contact_regex], max_length=17, null=False, blank=True, help_text="Phone Number", default="")

    contact_number = models.CharField(max_length=17, null=False, blank=True, help_text="Phone Number", default="")

    address = models.TextField(null=True, blank=True, help_text="Address")

    lat = models.FloatField(_("Latitude"), blank=True, default=0.0)

    lon = models.FloatField(_("Longitude"), blank=True, default=0.0)

    is_otp_verified = models.BooleanField(_("OTP Verified"), default=False)

    created_date = models.DateField(auto_now=False, auto_now_add=True)

    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Personal Infos"

    def __str__(self):
        return self.user.email if self.user.email != '' else str(self.user.id)

    def calculated_age(self):
        return relativedelta(date.today(), self.date_of_birth).years

    def full_name(self):
        return self.first_name + " " + self.last_name if not self.first_name == '' or not self.last_name == '' else ''
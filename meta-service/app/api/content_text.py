from fastapi import APIRouter, HTTPException

from app.models import UrlRequest, TextResponse, BarcodeResponse
from app.services.content import ContentObject

router = APIRouter()


@router.post("/find-text", response_model=TextResponse)
async def find_texts_from_media_content_url(req: UrlRequest):
    content = ContentObject(url=req.url)
    data = content.find_texts()
    return TextResponse(count=len(data), data=data)


@router.post("/scan-barcode", response_model=BarcodeResponse)
async def scan_barcode_from_media_content_url(req: UrlRequest):
    content = ContentObject(url=req.url)
    data = content.scan_barcode()
    return BarcodeResponse(count=len(data), data=data)
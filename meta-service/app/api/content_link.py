from fastapi import APIRouter, HTTPException

from app.models import ContentLinkRequest, ContentLinkResponse, TagResponse
from app.services.content import ContentManagerResolver

router = APIRouter()


@router.post("/{platform_code}", response_model=ContentLinkResponse)
async def get_media_content_url_from_post_url(platform_code: str, url_req: ContentLinkRequest):
    try:
        manager = ContentManagerResolver(url=url_req.url).get_manager_for(code=platform_code)
        content, payload = await manager.get_content()

        if url_req.include_tags and content:
            tags = content.get_tags()
            payload['tags'] = TagResponse(count=len(tags), data=tags)

        return ContentLinkResponse(**payload)

    except Exception as ex:
        print(ex)
        raise HTTPException(status_code=400, detail=ex.message)
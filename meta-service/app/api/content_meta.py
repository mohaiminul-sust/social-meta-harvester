from fastapi import APIRouter, HTTPException

from app.models import UrlRequest, TagResponse, CategoryResponse, ColorResponse
from app.services.content import ContentObject

router = APIRouter()


@router.post("/generate-media-tags", response_model=TagResponse)
async def get_tags_from_media_content_url(req: UrlRequest):
    content = ContentObject(url=req.url)
    tags = content.get_tags()
    return TagResponse(count=len(tags), data=tags)


@router.post("/generate-media-category", response_model=CategoryResponse)
async def get_content_category_from_media_content_url(req: UrlRequest):
    content = ContentObject(url=req.url)
    cat = content.get_personal_category()
    return CategoryResponse(data=cat)


@router.post("/check-content-safety", response_model=CategoryResponse)
async def check_content_safety_from_media_content_url(req: UrlRequest):
    content = ContentObject(url=req.url)
    cat = content.get_safe_category()
    return CategoryResponse(data=cat)


@router.post("/find-colors", response_model=ColorResponse)
async def find_colors_from_media_content_url(req: UrlRequest):
    content = ContentObject(url=req.url)
    color_data = content.scan_colors()
    return ColorResponse(**color_data)
from fastapi import APIRouter
from app.settings import get_settings

settings = get_settings()
router = APIRouter()


@router.get("/__health")
async def health_check():
    return {
        "status": "OK",
        "service": settings.APP_NAME,
        "version": settings.APP_VERSION
    }
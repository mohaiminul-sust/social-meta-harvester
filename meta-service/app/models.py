from typing import Optional, List
from pydantic import BaseModel


class UrlRequest(BaseModel):
    url: str


class ContentLinkRequest(UrlRequest):
    include_tags: Optional[bool] = False


class UrlResponse(BaseModel):
    morphed_url: Optional[str]
    store_url: Optional[str]


class ObjectResponse(BaseModel):
    name: str
    match: float


class TagResponse(BaseModel):
    count: int
    data: List[ObjectResponse]


class ContentLinkResponse(BaseModel):
    content_url: str
    save_content: bool
    links: Optional[UrlResponse]
    tags: Optional[TagResponse]


class CategoryResponse(BaseModel):
    data: ObjectResponse


class CoordinatesResponse(BaseModel):
    xmin: int
    ymin: int
    xmax: int
    ymax: int
    height: int
    width: int


class TextObjectResponse(BaseModel):
    data: str
    coordinates: CoordinatesResponse

class TextResponse(BaseModel):
    count: int
    data: List[TextObjectResponse]


class BarcodeObjectResponse(BaseModel):
    data: str
    x1: int
    y1: int
    x2: int
    y2: int


class BarcodeResponse(BaseModel):
    count: int
    data: List[BarcodeObjectResponse]


class ColorObjectResponse(BaseModel):
    closest_palette_color: str
    closest_palette_color_html_code: str
    closest_palette_color_parent: str
    closest_palette_distance: float
    html_code: str
    percent: float
    r: int
    g: int
    b: int


class ColorResponse(BaseModel):
    color_percent_threshold: float
    color_variance: int
    object_percentage: float
    background_colors: List[ColorObjectResponse]
    foreground_colors: List[ColorObjectResponse]
    image_colors: List[ColorObjectResponse]
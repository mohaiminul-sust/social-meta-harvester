from typing import Optional, List, Tuple, Callable
from enum import Enum
from urllib import request
import time, os, requests, aiofiles

FILE_STORE_PATH = '/temp/temp_%s.jpg'

get_temp_file_path: Callable[[], str] = lambda : FILE_STORE_PATH % f'{int(time.time())}'

get_url_from_redirection: Callable[[str, ], str] = lambda url: request.urlopen(url).geturl()

get_content_from_url: Callable[[str, ], bytes] = lambda url: requests.get(url).content


class CategorizerType(Enum):
    SAFE_CHECK = 'nsfw_beta'
    PERSONAL = 'personal_photos'


async def save_content_on_disk(c_url: str) -> str:
    img_data = get_content_from_url(c_url)
    temp_path = get_temp_file_path()

    os.makedirs(os.path.dirname(temp_path), exist_ok=True)

    async with aiofiles.open(temp_path, 'wb') as out_file:
        # content = await img_data.read()  # async read
        await out_file.write(img_data)

    return temp_path
from fastapi import FastAPI

from app.api import ping, content_link, content_meta, content_text
from app.settings import get_settings

settings = get_settings()

app = FastAPI(
    title=settings.APP_NAME,
    description=settings.APP_DESCRIPTION,
    version=settings.APP_VERSION,
    contact=settings.MAINTAINER_CONTACT,
    license_info=settings.LICENSE,
    openapi_url="/openapi.json",
)

app.include_router(ping.router, tags=["ping"])
app.include_router(content_link.router, prefix="/content-link", tags=["link grabber"])
app.include_router(content_meta.router, prefix="/content-meta", tags=["meta generation"])
app.include_router(content_text.router, prefix="/content-text", tags=["text recognition"])
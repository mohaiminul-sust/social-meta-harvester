from typing import List, Tuple
from pydantic import BaseSettings
from functools import lru_cache

import os

IMAGGA_API_VERSION = os.environ.get('IMAGGA_API_VERSION', 'v2')
IMAGGA_API_KEY = os.environ.get('IMAGGA_API_KEY')
IMAGGA_API_SECRET = os.environ.get('IMAGGA_API_SECRET')


class ServiceSettings(BaseSettings):
    APP_NAME: str = "Meta Service"
    APP_VERSION: str = "1.0.1"
    APP_DESCRIPTION: str = "Social media content link grabber and meta tag generation, categorization service APIs"
    ALLOWED_CONTENT_SERVICES: List[str] = ['instagram', 'facebook', 'flickr', ]
    IMAGGA_API_BASE_URL: str = f"https://api.imagga.com/{IMAGGA_API_VERSION}"
    IMAGGA_AUTH_PAIR: Tuple[str, str] = (IMAGGA_API_KEY, IMAGGA_API_SECRET)
    MAINTAINER_CONTACT: dict = {
        "name": "Mohaiminul Islam",
        "email": "mohaiminul.sust@gmail.com"
    }
    LICENSE: dict = {
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    }


@lru_cache
def get_settings() -> ServiceSettings:
    return ServiceSettings()
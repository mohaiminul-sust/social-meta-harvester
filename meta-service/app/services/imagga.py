from typing import Optional, List, Tuple, Union
from enum import Enum

from app.utils import CategorizerType
from app.settings import get_settings

import requests, operator

settings = get_settings()


class ImaggaRoute(Enum):
    TAGS = f"{settings.IMAGGA_API_BASE_URL}/tags"
    CATS = f"{settings.IMAGGA_API_BASE_URL}/categories/%s"
    COLORS = f"{settings.IMAGGA_API_BASE_URL}/colors"
    TEXT = f"{settings.IMAGGA_API_BASE_URL}/text"
    BARCODE = f"{settings.IMAGGA_API_BASE_URL}/barcodes"


class ImaggaRequestManager(object):
    path: str = ""
    auth_pair: Tuple[str, str] = ("", "")

    def __init__(self, path: str):
        self.path = path
        self.auth_pair = settings.IMAGGA_AUTH_PAIR

    def get_response_for(self, url: str) -> dict:
        res =  requests.post(url, auth=self.auth_pair, files={
            'image': open(self.path, 'rb')
        })
        return res.json()


class MetaGenerator(object):
    def __init__(self, path: str):
        self.handler = ImaggaRequestManager(path)

    def _format_score(self, score: Union[str, int, float]) -> float:
        if type(score) == float:
            return round(score, 2)
        return round(float(score), 2)

    def _get_sorted_list_by_score(self, x: dict) -> List[Tuple[str, float]]:
        return sorted(x.items(), key=operator.itemgetter(1), reverse=True)

    def _formatted_payload_object(self, entry: Tuple[str, float]) -> dict:
        return {
            "name": entry[0],
            "match": entry[1],
        }

    def get_tags(self) -> List[dict]:
        tag_res = self.handler.get_response_for(ImaggaRoute.TAGS.value)
        tags = tag_res['result']['tags']

        mapping= {}
        for tag in tags:
            mapping[tag['tag']['en']] = self._format_score(tag['confidence'])

        tags = self._get_sorted_list_by_score(mapping)
        payload = [ self._formatted_payload_object(t_obj) for t_obj in tags ]
        return payload

    def get_cat_for_content(self, cat_type: CategorizerType) -> dict:
        url = ImaggaRoute.CATS.value % cat_type.value
        cat_res = self.handler.get_response_for(url)
        cats = cat_res['result']['categories']

        mapping= {}
        for cat in cats:
            mapping[cat['name']['en']] = self._format_score(cat['confidence'])

        cats = self._get_sorted_list_by_score(mapping)
        return self._formatted_payload_object(cats[0])

    def scan_colors(self) -> dict:
        res = self.handler.get_response_for(ImaggaRoute.COLORS.value)
        data = res['result']['colors']
        return data


class MetaDetector(object):
    def __init__(self, path: str):
        self.handler = ImaggaRequestManager(path)

    def find_texts(self) -> List[dict]:
        res = self.handler.get_response_for(ImaggaRoute.TEXT.value)
        data = res['result']['text']
        return data

    def scan_barcode(self) -> List[dict]:
        res = self.handler.get_response_for(ImaggaRoute.BARCODE.value)
        data = res['result']['barcodes']
        return data
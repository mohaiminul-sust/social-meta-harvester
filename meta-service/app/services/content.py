from typing import Optional, List, Tuple, Union

from app.utils import save_content_on_disk, get_url_from_redirection, CategorizerType
from app.settings import get_settings
from .imagga import MetaGenerator, MetaDetector

import requests

settings = get_settings()


class ContentObject(object):
    generator: MetaGenerator
    detector: MetaDetector

    def __init__(self, url: str):
        self.generator = MetaGenerator(path=url)
        self.detector = MetaDetector(path=url)

    def get_tags(self) -> List[dict]:
        return self.generator.get_tags()

    def get_safe_category(self) -> dict:
        return self.generator.get_cat_for_content(CategorizerType.SAFE_CHECK)

    def get_personal_category(self) -> dict:
        return self.generator.get_cat_for_content(CategorizerType.PERSONAL)

    def find_texts(self) -> List[dict]:
        return self.detector.find_texts()

    def scan_barcode(self) -> List[dict]:
        return self.detector.scan_barcode()

    def scan_colors(self) -> dict:
        return self.generator.scan_colors()


class ContentManager(object):
    url: str = ""
    c_url: str = ""
    s_url: Optional[str] = None
    save_content: bool = True
    content: Optional[ContentObject] = None

    def __init__(self, url: str):
        self.url = url
        self.s_url = url
        self._reset_content()
        self.c_url = url

    def _reset_content(self):
        self.content = ContentObject(url=self.s_url) if self.s_url else None

    async def _store_content(self):
        self.s_url = await save_content_on_disk(self.c_url)
        self._reset_content()

    def craft_link(self) -> Tuple[str, dict]:
        return self.c_url, {}

    async def get_content(self) -> Tuple[Optional[ContentObject], dict]:
        link, payload = self.craft_link()
        self.s_url = link

        if self.save_content:
            await self._store_content()

        payload['store_url'] = self.s_url

        return self.content, {
            "content_url": link,
            "save_content": self.save_content,
            "links": payload
        }


class InstagramContentManager(ContentManager):
    size: Optional[str] = None

    def __init__(self, url: str, size: Optional[str] = None):
        self.size = size if size else 'm'
        super(InstagramContentManager, self).__init__(url=url)

    def get_morphed_url(self) -> str:
        return f'{self.url}/media/?size={self.size}'

    def craft_link(self) -> Tuple[str, dict]:
        m_url = self.get_morphed_url()
        self.c_url = get_url_from_redirection(m_url)

        return self.c_url, {
            "morphed_url": m_url,
        }


class FacebookContentManager(ContentManager):
    pass


class FlickrContentManager(ContentManager):
    pass


class ContentManagerResolver(object):
    url: str = ""
    services: List[str] = settings.ALLOWED_CONTENT_SERVICES

    def __init__(self, url: str):
        self.url = url

    def get_manager_for(self, code: str):
        if code not in self.services:
            raise Exception(f"Platform not supported! \
                Only {', '.join(self.services)} are supported")

        if code == 'instagram':
            return InstagramContentManager(url=self.url, size='m')
        elif code == 'facebook':
            return FacebookContentManager(url=self.url)
        elif code == 'flickr':
            return FlickrContentManager(url=self.url)

        return ContentManager(url=self.url)
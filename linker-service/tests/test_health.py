from starlette.testclient import TestClient
from app.main import app

client = TestClient(app)


def test_health(test_app):
    expected_resp = {
        "status": "OK"
    }

    response = test_app.get("/__health/")

    assert response.status_code == 200
    assert response.json() == expected_resp
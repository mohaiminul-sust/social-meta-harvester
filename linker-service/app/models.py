from typing import Optional, List
from pydantic import BaseModel

class UrlBaseObject(BaseModel):
    target_url: str


class UrlObject(UrlBaseObject):
    is_active: bool
    clicks: int
    created_by: Optional[dict]

    class Config:
        orm_mode = True


class UrlInfoObject(UrlObject):
    url: str
    admin_url: str
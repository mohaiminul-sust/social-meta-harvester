from fastapi import FastAPI

from app.api import ping, shorten_link
from app.settings import get_settings, init_logger
from app.database import Base, engine
from app.auth.middlewares import JwtAuthUserMiddleware

import logging

init_logger()

settings = get_settings()
logging.info(f"Starting {settings.APP_NAME}")

app = FastAPI(
    title=settings.APP_NAME,
    description=settings.APP_DESCRIPTION,
    version=settings.APP_VERSION,
    contact=settings.MAINTAINER_CONTACT,
    license_info=settings.LICENSE,
    openapi_url="/openapi.json",
)

Base.metadata.create_all(bind=engine)

app.include_router(ping.router, tags=["ping"])
app.include_router(shorten_link.router, tags=["linker utils"])

app.add_middleware(JwtAuthUserMiddleware, applied_routes=['/shorten-url'])

logging.info(f"{settings.APP_NAME} Started!")
from typing import List, Tuple
from pydantic import BaseSettings
from functools import lru_cache

from fastapi.logger import logger as fastapi_logger
from logging.handlers import RotatingFileHandler

import os, logging

LOG_STORE_PATH = 'logfile.log'


class ServiceSettings(BaseSettings):
    APP_NAME: str = "Linker Service"
    APP_VERSION: str = "1.0.0"
    APP_DESCRIPTION: str = "Link utility and link tree mapper service"
    MAINTAINER_CONTACT: dict = {
        "name": "Mohaiminul Islam",
        "email": "mohaiminul.sust@gmail.com"
    }
    LICENSE: dict = {
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    }
    DB_URL: str = os.environ.get('DB_URL')


@lru_cache
def get_settings() -> ServiceSettings:
    return ServiceSettings()


def init_logger():
    logging.basicConfig(filename=LOG_STORE_PATH, encoding='utf-8', level=logging.DEBUG)

    handler = RotatingFileHandler(LOG_STORE_PATH, backupCount=0)
    fastapi_logger.addHandler(handler)

    log_formatter = logging.Formatter("[%(asctime)s.%(msecs)03d] %(levelname)s [%(thread)d] - %(message)s", "%Y-%m-%d %H:%M:%S")
    handler.setFormatter(log_formatter)
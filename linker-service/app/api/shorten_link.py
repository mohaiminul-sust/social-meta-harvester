from fastapi import APIRouter, HTTPException, Depends, Request
from fastapi.responses import RedirectResponse
from sqlalchemy.orm import Session

import validators, logging

from app.models import UrlBaseObject, UrlInfoObject
from app.settings import get_settings
from app.database import get_db
from app.services import UrlManager

settings = get_settings()
router = APIRouter()


@router.post("/shorten-url", response_model=UrlInfoObject)
async def generate_shortened_url(url_req: UrlBaseObject, request: Request, db: Session = Depends(get_db)):
    # logging.info(f'Authenticated : {request.state.is_authenticated}')
    # logging.info(f'payload received from : {request.state.user}')
    if not validators.url(url_req.target_url):
        raise HTTPException(status_code=401, detail="Your provided URL is not valid")

    mgr = UrlManager(db=db)
    entry = mgr.generate_shortened_url(
        target_url=url_req.target_url,
        created_by=dict(request.state.user)
    )
    return entry


@router.get("/{url_key}")
async def forward_to_target_url(url_key: str, request: Request, db: Session = Depends(get_db)):
    mgr = UrlManager(db=db)
    db_entry = mgr.fetch_url_from_db(url_key=url_key)

    if not db_entry:
        raise HTTPException(status_code=404, detail=f"URL '{request.url}' doesn't exist")

    mgr.increase_url_click_count(url=db_entry)

    return RedirectResponse(db_entry.target_url)
from typing import Optional, List, Tuple, Callable
from enum import Enum
from urllib import request
from fastapi import HTTPException

import time, os, secrets, string


FILE_STORE_PATH = '/temp/temp_%s.jpg'

get_temp_file_path: Callable[[], str] = lambda : FILE_STORE_PATH % f'{int(time.time())}'

get_url_from_redirection: Callable[[str, ], str] = lambda url: request.urlopen(url).geturl()

get_random_char: Callable[[], str] = lambda : secrets.choice(string.ascii_uppercase + string.digits)

create_random_key: Callable[[int, ], str] = lambda length: "".join([ get_random_char() for _ in range(length) ])
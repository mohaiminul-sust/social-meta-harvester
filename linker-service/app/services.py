from typing import Optional, List
from sqlalchemy.orm import Session

from app.schemes import URL
from app.utils import create_random_key


class UrlManager(object):
    def __init__(self, db: Session):
        self.db = db

    def fetch_url_from_db(self, url_key: str) -> URL:
        return (
            self.db.query(URL)
            .filter(URL.key == url_key)
            .first()
        )

    def fetch_admin_url_from_db(self, admin_key: str) -> URL:
        return (
            self.db.query(URL)
            .filter(URL.secret_key == admin_key)
            .first()
        )

    def _update_url_entry(self, url: URL):
        self.db.commit()
        self.db.refresh(url)

    def _add_url_entry(self, url: URL):
        self.db.add(url)
        self._update_url_entry(url)

    def _get_unique_random_key(self, length: int) -> str:
        key = create_random_key(length=length)
        if not self.fetch_url_from_db(url_key=key):
            return key
        return self._get_unique_random_key(length=length)

    def _store_url_object(self, url: str, key: str, secret: str, created_by: Optional[dict]) -> URL:
        db_entry = URL(
            target_url=url,
            key=key,
            secret_key=secret,
            created_by=created_by
        )
        self._add_url_entry(db_entry)
        return db_entry

    def generate_shortened_url(self, target_url: str, created_by: Optional[dict]) -> URL:
        key = self._get_unique_random_key(length=5)
        secret_key = f"{key}_{create_random_key(length=8)}"

        db_entry = self._store_url_object(
            url=target_url,
            key=key,
            secret=secret_key,
            created_by=created_by
        )
        db_entry.url = key
        db_entry.admin_url = secret_key
        return db_entry

    def increase_url_click_count(self, url: URL) -> URL:
        url.clicks += 1
        self._update_url_entry(url)
        return url

    def _toggle_url_active_status(self, url: URL, active: bool) -> URL:
        url.is_active = active
        self._update_url_entry(url)
        return url

    def deactivate_url(self, url_key: str) -> URL:
        url = self.fetch_url_from_db(url_key=url_key)
        if url and url.is_active:
            url = self._toggle_url_active_status(url=url, active=False)
        return url

    def activate_url(self, url_key: str) -> URL:
        url = self.fetch_url_from_db(url_key=url_key)
        if url and not url.is_active:
            url = self._toggle_url_active_status(url=url, active=True)
        return url

from pydantic import BaseModel
from typing import List, Optional
from jose import jwt
from jose.exceptions import JOSEError

from fastapi import HTTPException, Request

import os, logging


# base classes
class AuthUser(BaseModel):
    user_id: str
    username: str
    fullname: str
    email: str
    user_role: str


class AuthUserManager(object):
    user: Optional[AuthUser] = None
    user_token: Optional[str] = None

    @property
    def is_authenticated(self) -> bool:
        return self.user_token is not None

    def decode_user_from_request(self, request: Request):
        # override based on auth strategy and set the user and user_token :)
        pass


# For JWT based Auth user
class JwtAuthUserManager(AuthUserManager):
    def _get_validated_token(self, headers: dict) -> str:
        if 'authorization' not in headers.keys():
            raise HTTPException(status_code=401, detail="authorization key missing in header")

        auth = headers["authorization"]
        auth_parts = auth.split(" ")

        if len(auth_parts) != 2:
            raise HTTPException(status_code=401, detail="Invalid authorization key in header")

        if str(auth_parts[0]).lower() != "bearer":
            raise HTTPException(status_code=401, detail="Token Header mismatch")

        return str(auth_parts[1])

    def _get_decoded_data(self, token: str) -> dict:
        try:
            payload = jwt.decode(
                token,
                key=os.environ.get('JWT_SECRET'),
                algorithms=['HS256'],
                options={
                    "verify_signature": True,
                    "verify_aud": False,
                    "verify_iss": False
                }
            )
            logging.info("payload => ", payload)
            return payload

        except Exception as e:
            raise HTTPException(status_code=401, detail=str(e.message))

    def decode_user_from_request(self, request: Request):
        headers = request.headers
        token = self._get_validated_token(headers)
        self.user_token = token
        decoded_data = self._get_decoded_data(token)
        self.user = AuthUser(**decoded_data)
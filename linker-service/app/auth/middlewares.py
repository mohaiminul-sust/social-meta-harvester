
from typing import List, Optional

from starlette.middleware.base import BaseHTTPMiddleware
from fastapi import Request

import os, logging
from .services import AuthUserManager, JwtAuthUserManager


# base class
class AuthUserMiddleware(BaseHTTPMiddleware):
    applied_routes: List[str] = []
    manager: AuthUserManager = AuthUserManager()

    def __init__(self, app, applied_routes: List[str]=[]):
        super().__init__(app)
        self.applied_routes = applied_routes

    def enrich_request_user(self, request: Request) -> Request:
        if request["path"] in self.applied_routes:
            self.manager.decode_user_from_request(request)

        request.state.user = self.manager.user
        request.state.is_authenticated = self.manager.is_authenticated
        return request

    async def dispatch(self, request: Request, call_next):
        # print(request.__dict__)
        request = self.enrich_request_user(request)
        response = await call_next(request)
        return response



# For JWT based Auth user
class JwtAuthUserMiddleware(AuthUserMiddleware):
    def __init__(self, app, applied_routes: List[str]=[]):
        super().__init__(app, applied_routes=applied_routes)
        self.manager = JwtAuthUserManager()